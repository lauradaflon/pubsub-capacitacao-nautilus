#!/usr/bin/env python

import rospy
from std_msgs.msg import Int64

def counter():
    pub = rospy.Publisher('chatter', Int64, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        for i in range(1,11):
            num_int = i
            rospy.loginfo(num_int)
            pub.publish(num_int)
            rate.sleep()
if __name__ == '__main__':
    try:
        counter()
    except rospy.ROSInterruptException:
        pass



